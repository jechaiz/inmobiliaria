/*
 * 
 * 
 * 
 */
package inmobiliaria;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.csvreader.CsvWriter;

/**
 *
 * @author Julian Echaiz
 */
public class Inmobiliaria {
    static Scanner sc = new Scanner(System.in);
    String emailPattern = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@" + "[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
    Pattern pattern = Pattern.compile(emailPattern);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //
        //Llamada al menu
        Inmobiliaria main = new Inmobiliaria();
        main.menu();
    }
    public void menu(){
        String opcion="";
        while(!opcion.equals("3")){
              System.out.println("╔                  Menu                     ╗");
              System.out.println("║ 1. Cotizar casas                          ║");
              System.out.println("║ 2. Entrar Sistema Inmobiliaria            ║");
              System.out.println("║ 3. Salir                                  ║");
              System.out.println("╚                                           ╝");
              System.out.print("Ingrese opcion: ");      
              opcion = sc.nextLine();
            switch (opcion){
                case "1":
                    cotizarCasas();
                    break;
                case "2":
                    entrarSistemaInmobiliaria();
                    break;
                default:
                    System.out.println("Opcion No valida!!");
            }
        }
        sc.close();
    }
    
    public void cotizarCasas(){
        float tipoI=7.9f, numerador,denominador,cuotaMaxima, valorMaxPrestamo;
        int periodoFijo=15;
        System.out.println("Bienvenido/a este es el sistema de cotizacion en linea, primero, necesito tu nombre. ");
        String nombre = sc.nextLine();
        System.out.print("Mucho gusto! "+nombre+", ahora necesito tu correo electronico. ");
        Boolean mailVer=false;
        while(mailVer==false){
            String correo = sc.nextLine();
            Matcher matcher = pattern.matcher(correo);
                if (matcher.matches()) {
                    System.out.println("Bien, ese correo es valido. ¿Buscas casa o departamento?");
                    mailVer=true;
                }
                else{
                    System.out.println("Oh no! me parece que ese correo no es valido... vuelve a intentarlo.");        
                    }
        }
        
        String vivienda = sc.nextLine().toLowerCase();
        while(!(vivienda.equals("casa")) && !(vivienda.equals("departamento"))){
            System.out.println("Por favor, indicame si buscas una casa o departamento.");
            vivienda = sc.nextLine();
        }
        String nPlantas="";
        if(vivienda=="casa"){
            System.out.println("¿De cuántas plantas(1/2)? Puedes escribir “omitir” en caso de no tener preferencia.");
            nPlantas = sc.nextLine();            
            while(nPlantas.toLowerCase()!="omitir" && nPlantas!="1" && nPlantas!="2"){
                System.out.println("Ha ingreaso un valor incorrecto.\n¿De cuántas plantas(1/2)? Puedes escribir “omitir” en caso de no tener preferencia. ");
                nPlantas = sc.nextLine();
            }
        }
        
        System.out.println("¿Número mínimo de Cuartos? Puedes escribir “omitir” en caso de no tener preferencia");
        String nCuartos = sc.nextLine();
        System.out.println("¿De que sector le gustaria que sea su "+vivienda+"?" );
        String sector = sc.nextLine();
        System.out.println("Bien, y ¿de que cantidad es su ingreso mensual?");
        float ingresoMens = sc.nextFloat();
        System.out.println("Y, ¿cuanto estaria dispuesto/a a dar de entrada?");
        float entradaDin = sc.nextFloat();
        if(vivienda.equals("casa")){
            Casa casa1 = new Casa("Modelo E", nPlantas, nCuartos);
            CsvWriter csvWriter=new CsvWriter("casas.csv");
            
            cuotaMaxima=ingresoMens*(0.40f);
            tipoI=(tipoI)/(1200);
            numerador=(((float)(Math.pow((1+tipoI),(12*15))))-1);
            denominador=(tipoI)*(((float)(Math.pow((1+tipoI),(12*15)))));
            valorMaxPrestamo=cuotaMaxima*((numerador)/(denominador));
            float sueldoEntrada=valorMaxPrestamo+entradaDin;
            System.out.printf("\nEl valor máximo prestado es: %.3f", valorMaxPrestamo);
            System.out.printf("\nEl valor total es: %.3f", sueldoEntrada);
        }
        else{
            //Departamento Dep1 = new Departamento();
        }
        System.out.println("\nEn base a tus respuestas tenemos las siguientes opciones:");
        System.out.println("Deseas información de algún modelo en especial, te lo enviaremos a tu correo selecciona por favor:");
        System.out.println("¡Información ha sido enviada a tu correo, Gracias por cotizar con nosotros!");
    }
    
    public void entrarSistemaInmobiliaria(){
        System.out.println("Para entrar al Sistema debe crearse un usuario.");
        System.out.print("Usuario: ");
        String userName = sc.nextLine();
        String contrasena ="";
        while(contrasena.length()<6){
            System.out.print("Contraseña(Minimo 6 caracteres): ");
            contrasena = sc.nextLine();
        }
        System.out.println("Cuenta creada exitosamente.");
        
        String opcion="";
        while(!opcion.equals("4")){
              System.out.println("╔                   Menu                      ╗");
              System.out.println("║Usted podra crear y modificar Urbanizaciones,║");
              System.out.println("║Etapas, Lotes y Casas/Departamentos.         ║");
              System.out.println("║                                             ║");
              System.out.println("║ 1. Crear                                    ║");
              System.out.println("║ 2. modificar                                ║");
              System.out.println("║ 3. Eliminar                                 ║");
              System.out.println("║ 4. Salir                                    ║");
              System.out.println("╚                                             ╝");
              System.out.print("Ingrese opcion: ");      
              opcion = sc.nextLine();
            switch (opcion){
                case "1":
                    crear();
                    break;
                case "2":
                    modificar();
                    break;
                case "3":
                    eliminar();
                    break;
                default:
                    System.out.println("Opcion No valida!!");
            }
        }
    }
    public void crear(){
        
    }
    
    public void modificar(){
        
    }
    
    public void eliminar(){
        
    }
}
