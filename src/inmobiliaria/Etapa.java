/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmobiliaria;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Anthony Loor
 */
public class Etapa {
    private String nombre;
    private boolean estado;
    private List<Manzana> manzanas = new ArrayList<>();
    private List<Departamento> departamentos = new ArrayList<>();
    private List<String> servicios = new ArrayList<>(); //buscar como leer el archivo de servicios para importar los servicios.
    
    public Etapa(String nombre, boolean estado, List<Manzana> manzanas, List<Departamento> departamentos){
        this.nombre=nombre;
        this.estado=estado;
        this.manzanas=manzanas;
        this.departamentos=departamentos;
    }
    public boolean estaDisponible(boolean estado){
        return this.estado;
    }
}
