/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmobiliaria;

/**
 *
 * @author Anthony Loor
 */
public class Departamento extends Casa{
    private boolean parqueadero;
    private boolean lavanderia;
    private boolean ascensor;
    
    public boolean tieneParqueadero(boolean parqueadero){
        return this.parqueadero;
    }
    public boolean tieneLavanderia(boolean lavanderia){
        return this.lavanderia;
    }
    public boolean tieneAscensor(boolean ascensor){
        return this.ascensor;
    }
    
    public Departamento(String nombre, String nPlantas, String nCuartos, boolean cocina, boolean cuartoDeServicio, String nBanos, float tamano, float precio, String rutaImagenPlanos, boolean parqueadero, boolean lavanderia, boolean ascensor){
        super(nombre, nPlantas, nCuartos, cocina, cuartoDeServicio, nBanos, tamano, precio, rutaImagenPlanos);
        //hacer gets & sets de los atributos de casa para acceder a ellos en esta clase y hacer el this.getNombre()=nombre;
        this.parqueadero=parqueadero;
        this.lavanderia=lavanderia;
        this.ascensor=ascensor;
    }
}
