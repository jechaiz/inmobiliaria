/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmobiliaria;

/**
 *
 * @author Anthony Loor
 */
public class Casa {
    private String nombre;
    private String nPlantas;
    private String nCuartos;
    private boolean cocina;
    private boolean cuartoDeServicio;
    private String nBanos;
    private float tamano;
    private float precio;
    private String rutaImagenPlanos;
    
    public Casa(String nombre, String nPlantas, String nCuartos, boolean cocina, boolean cuartoDeServicio, String nBanos, float tamano, float precio, String rutaImagenPlanos){
        this.nombre=nombre;
        this.nPlantas=nPlantas;
        this.nCuartos=nCuartos;
        this.cocina=cocina;
        this.cuartoDeServicio=cuartoDeServicio;
        this.nBanos=nBanos;
        this.tamano=tamano;
        this.precio=precio;
        this.rutaImagenPlanos=rutaImagenPlanos;
    }
    public Casa(String nombre, String nPlantas, String nCuartos){
        this.nombre=nombre;
        this.nPlantas=nPlantas;
        this.nCuartos=nCuartos;
    }
    
}
