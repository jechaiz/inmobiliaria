/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmobiliaria;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Anthony Loor
 */
public class Urbanizacion {
    private String nombre;
    private String direccion;
    private List<Etapa> etapas = new ArrayList<>();
    private List<Casa> casas = new ArrayList<>();
    
    public Urbanizacion(String nombre, String direccion, List<Etapa> etapas, List<Casa> casas){
        this.nombre=nombre;
        this.direccion=direccion;
        this.etapas=etapas;
        this.casas=casas;
    }
}
